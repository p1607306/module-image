CC=g++
INCLUDES=-I/usr/include/SDL2
LIBS=-lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer -lm

CFLAGS=-Wall -ggdb

all: bin/test bin/exemple bin/affichage 

bin/affichage: obj/mainAffichage.o obj/Image.o obj/Pixel.o
	$(CC) -ggdb -o bin/affichage obj/mainAffichage.o obj/Image.o obj/Pixel.o  $(LIBS)

bin/exemple: obj/mainExemple.o obj/Image.o obj/Pixel.o
	$(CC) -ggdb -o bin/exemple obj/mainExemple.o obj/Image.o obj/Pixel.o $(LIBS)

bin/test: obj/mainTest.o obj/Image.o obj/Pixel.o
	$(CC) -ggdb -o bin/test obj/mainTest.o obj/Image.o obj/Pixel.o  $(LIBS)

obj/mainAffichage.o: src/mainAffichage.cpp src/Image.h src/Pixel.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/mainAffichage.cpp 

obj/mainExemple.o: src/mainExemple.cpp src/Image.h src/Pixel.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/mainExemple.cpp 

obj/mainTest.o: src/mainTest.cpp src/Image.h src/Pixel.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/mainTest.cpp

obj/Image.o:src/Image.h src/Image.cpp src/Pixel.h
	$(CC) $(CFLAGS) $(INCLUDES) -c src/Image.cpp

obj/Pixel.o:src/Pixel.h src/Pixel.cpp 
	$(CC) $(CFLAGS) $(INCLUDES) -c src/Pixel.cpp

clean:
	rm obj/*.o

veryclean:
	 rm bin/*
doc:
	doxygen doc/image.doxy
