/** 
 \file Image.h
 \brief Génere une image composée de pixels .On peut donc créer une image,y insérer des pixels (setPix),accéder à un pixel (getPix),dessiner un rectangle sauvergarder l'image dans un fichier ,créer une image à partie des informations contenu dans un fichier et afficher l'image dans une fenêtre SDL.

 \version 1.0
 \date 2019/02/13 
 \author p1811469 & p1607306
*/

#ifndef _IMAGE_H
#define _IMAGE_H
#include "Pixel.h"
#include <string>


 class Image
 {

 private:
 	  Pixel * tab;
 	  unsigned int dimx,dimy;
    
    /** \brief Initialise la fenêtre de taille 200x200 et charge l'image dans la fenêtre */
    void afficherInit();

   /** \brief Detruit tout ce qui a été initialisé dans afficherInit() */
    void afficherDetruit();

    /** \brief Lecture au clavier pour quitter , zoomer et dézoomer */
    void afficherBoucle();

 public:
 	
 	/**

 	\brief Constructeur par défaut de la classe: initialise dimx et dimy à 0.
 	   Ce constructeur n'alloue pas de pixel
    */
  Image();
 	
 	/** 
 	  \brief Constructeur de la classe: initialise dimx et dimy (après vérification)
         puis alloue le tableau de pixel dans le tas (image noire)
        \param[out]  longueur de l'image
        \param[out] largeur de l'image 
    */
    Image(const unsigned int dimensionX,const unsigned int dimentionY);
 	
 	/**
 	   \brief Destructeur de la classe: déallocation de la mémoire du tableau de pixels
      et mise à jour des champs dimx et dimy à 0  */
   ~Image();
   
    /** 
    \brief récupère le pixel original de coordonnées (x,y) en vérifiant leur validité
        la formule pour passer d'un tab 2D à un tab 1D est tab[y*dimx+x] 
        \param[out] cordonnée x
        \param[out] cordonnée y
        */
    Pixel& getPix(const unsigned int x,const unsigned int y)const;
  
    /** \brief modifie le pixel de coordonnées (x,y)
        \param[out] cordonnée x
        \param[out] cordonnée y
        \param[in,out] Pixel à inserer */
     void setPix(const unsigned int x,const unsigned int y,const Pixel & couleur);
  
    /** 
     \brief Dessine un rectangle plein de la couleur dans l'image (en utilisant setPix, indices en paramètre compris) 
      \param[out] coordonnée x minimal
      \param[out] coordonnée y minimal
      \param[out] coordonnée x maximal
      \param[out] coordonnée y maximal
      \param[in,out] Pixel à inserer
      */
    void dessinerRectangle(const unsigned int Xmin,const unsigned int Ymin,const unsigned int Xmax,const unsigned int Ymax ,const Pixel & couleur);

    /**  \brief Efface l'image en la remplissant de la couleur en paramètre
     (en appelant dessinerRectangle avec le bon rectangle) 
     \param[in,out] Pixel à inserer
     */
    void effacer(const Pixel & couleur);

    /** \brief Effectue une série de tests vérifiant que le module fonctionne et
          que les données membres de l'objet sont conformes
    */
    void testRegression();
    
    /** \brief Sauvergarde l'image dans le fichier filename 
    \param[in] nom du fichier où l'on veut sauvegarder l'image
    */
    void sauver(const std::string & filename)const ;
    
    /**  \brief Crée l'image grâce au fichier filename,un peu comme un constructeur par copie
     \param[in] nom du fichier à lire
     */
    void ouvrir(const std::string & filename);
    
    /**  \brief Affiche l'image numériquement sur la console (pixel par pixel)*/
    void afficherConsole();
    
    /**  \brief Affiche ans une fenêtre 200x200 l'image en permettant de zoomer avec T, dézoomer avec G et fermer la fenêtre avec ESCAPE (echap) */
    void afficher();

 };

 #endif
