/** 
 \file Pixel.h
 \brief  Crée une couleur grâce au entier passée en paramètre

 \version 1.0
 \date  2019/02/13
 \author p1811469 & p1607306
*/


#ifndef _PIXEL_H
#define _PIXEL_H

class Pixel
{/** 
   \brief Données membres de la class Pixel composantes rouge,vert et bleu elles sont privées à la classe.*/
  private:
  	unsigned char r,g,b;

public:
     /**
	 \brief Initialise le pixel à la couleur noire
         Constructeur Pixel ()
	 */
	Pixel();
    /**
      \brief Constructeur de la classe: initialise r,g,b avec les paramètres 
     */
     Pixel(const unsigned int nr,const unsigned int ng,const unsigned int nb);
     /**
    \brief  Récupère la composante rouge du pixel
    */
	int getRouge()const;
    /**
    \brief  Récupère la composante vert du pixel
    */
	int getVert()const;
    /**
    \brief  Récupère la composante bleu du pixel
    */
	int getBleu()const;
    /**
    \brief Modifie la composante rouge du pixel
    */
	void setRouge(const unsigned int nr);
     /**
    \brief Modifie la composante vert du pixel
     
    */
	void setVert(const unsigned int ng);
    /*
     \brief Modifie la composante bleu du pixel
    */
    void setBleu(const unsigned int nb);
    
};
#endif
