#include "Pixel.h"
#include "Image.h"
#include <cassert>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <fstream>

using namespace std;

    SDL_Surface * surface;
    SDL_Texture * texture;
    bool has_changed;
    SDL_Window * window;
    SDL_Renderer * renderer;
    SDL_Rect r;

void Image::afficherInit(){
     // Initialisation de la SDL
    window=NULL;
    surface=NULL;
    renderer=NULL;
    texture=NULL;
    has_changed=false;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;SDL_Quit();exit(1);
    }

    if (TTF_Init() != 0) {
        cout << "Erreur lors de l'initialisation de la SDL_ttf : " << TTF_GetError() << endl;SDL_Quit();exit(1);
    }

    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << endl;SDL_Quit();exit(1);
    }

    window = SDL_CreateWindow("Image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 200, 200, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; SDL_Quit(); exit(1);
    }

    renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
     SDL_SetRenderDrawColor(renderer, 211, 211, 211, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    this->sauver("./data/image.ppm");
    SDL_Surface* image = IMG_Load("./data/image.ppm");
    if(!image)
    {
    cout<<"Erreur de chargement de l'image :"<<SDL_GetError()<<endl;SDL_Quit(); exit(1);
    }
    SDL_Surface * surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(image,SDL_PIXELFORMAT_ARGB8888,0);
    SDL_FreeSurface(surface);
    surface = surfaceCorrectPixelFormat;

    texture = SDL_CreateTextureFromSurface(renderer,surface);
    if (texture == NULL) {
        cout << "Error: problem to create the texture of image.ppm "<< endl;
        exit(1);
    }
    
    r.x = (200/2)-(dimx/2);
    r.y = (200/2)-(dimy/2);
    r.w = dimx;
    r.h = dimy;
    if (has_changed){
       SDL_UpdateTexture( texture, NULL, surface->pixels, surface->pitch);
      has_changed = false;
    }
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer,texture,NULL,&r);
    SDL_RenderPresent(renderer);
}

void Image::afficherBoucle(){

  SDL_Event events;
  bool quit = false;


while(!quit)
    {
     if (SDL_PollEvent(&events)) {
          if (events.type == SDL_QUIT) quit = true;           // Si l'utilisateur a clique sur la croix de fermeture
       if (events.type == SDL_KEYDOWN) {              // Si une touche est enfoncee
        switch (events.key.keysym.scancode) {
        case SDL_SCANCODE_T:   
            r.w+=5 ;
            r.h+=5 ; 
            r.x = (200/2)-(r.w/2);
            r.y = (200/2)-(r.w/2); 
            if (has_changed){

                 SDL_UpdateTexture( texture, NULL, surface->pixels, surface->pitch);
                 has_changed = false;
               }
               SDL_RenderClear(renderer);
               SDL_RenderCopy(renderer,texture,NULL,&r);
               SDL_RenderPresent(renderer);  
          break;
        case SDL_SCANCODE_G:
            r.w-=5;
            r.h-=5;
            r.x = (200/2)-(r.w/2);
            r.y = (200/2)-(r.h/2);
            if (has_changed){
                 SDL_UpdateTexture( texture, NULL, surface->pixels, surface->pitch);
                 has_changed = false;
               }
            SDL_RenderClear(renderer);
            SDL_RenderCopy(renderer,texture,NULL,&r);
            SDL_RenderPresent(renderer);
          ; 
          break;
        case SDL_SCANCODE_ESCAPE:
          quit = true;
          break;
        default: break;
        }
     }
     SDL_RenderClear(renderer);
   }
 }     
          
        
       SDL_RenderClear(renderer);
       SDL_RenderCopy(renderer, texture, NULL, NULL);
      SDL_RenderPresent(renderer);
}


void Image::afficherDetruit(){
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
}

Image::Image(){
	dimx=0;
	dimy=0;
    tab=NULL;

}

Image::Image(const unsigned int dimensionX,const unsigned int dimensionY){
     assert (dimensionX > 0 &&  dimensionY > 0);
	   dimx=dimensionX;
	   dimy=dimensionY;
	   tab=new Pixel[dimx*dimy];
}

Image::~Image(){
	delete [] tab;
	dimx=0;
	dimy=0;

}

Pixel& Image::getPix(const unsigned int x,const unsigned int y)const{
  assert(x>=0 && x<=dimx);
  assert(y>=0 && y<=dimy);
	Pixel& Pix=tab[y*dimx+x];
	return Pix;
}

void Image::setPix(const unsigned int x,const unsigned int  y,const Pixel & couleur){
	tab[y*dimx+x].setRouge(couleur.getRouge());
	tab[y*dimx+x].setVert(couleur.getVert());
	tab[y*dimx+x].setBleu(couleur.getBleu());
}

void Image::dessinerRectangle(const unsigned int Xmin,const unsigned int Ymin,const unsigned int Xmax,const unsigned int Ymax,const Pixel & couleur){
      assert(Xmin>0 && Ymin>0 && Xmax>0 && Ymax>0);
      for (unsigned int x=Xmin-1; x<Xmax;x++){
          for (unsigned int y=Ymin-1;y<Ymax;y++){
             setPix(x,y,couleur);
          }
      }
}

void Image::effacer(const Pixel & couleur){
       dessinerRectangle(1,1,dimx,dimy,couleur);


}

void Image::testRegression(){
      Pixel p(0,2,1);
      cout<<"------------"<<endl;
      cout<<p.getRouge();
      cout<<p.getVert();
      cout<<p.getBleu()<<endl;
      p.setRouge(4);
      p.setVert(4);
      p.setBleu(4);
      cout<<p.getRouge();
      cout<<p.getVert();
      cout<<p.getBleu()<<endl;
      cout<<"-------------"<<endl;
      Image Image1;
      Image monImage(20,15);
      for (int j=0;j<15;j++){
         for (int i=0;i<20;i++){
           monImage.setPix(i,j,p);
           }
      }
      for (int j=0;j<15;j++){
          for (int i=0;i<20;i++){
             cout<<monImage.getPix(i,j).getRouge();
             cout<<monImage.getPix(i,j).getVert();
             cout<<monImage.getPix(i,j).getBleu();
           }
           cout<<endl;
      }
      cout<<"-------------"<<endl;
      Pixel p1(3,3,3);
      Pixel p2(1,1,1);
      monImage.effacer(p1);
      for (int j=0;j<15;j++){
          for (int i=0;i<20;i++){
             cout<<monImage.getPix(i,j).getRouge();
             cout<<monImage.getPix(i,j).getVert();
             cout<<monImage.getPix(i,j).getBleu();
           }
           cout<<endl;
       }
      cout<<"-------------"<<endl;
      monImage.dessinerRectangle(2,7,10,9,p2);
      for (int j=0;j<15;j++){
          for (int i=0;i<20;i++){
             cout<<monImage.getPix(i,j).getRouge();
             cout<<monImage.getPix(i,j).getVert();
             cout<<monImage.getPix(i,j).getBleu();
           }
           cout<<endl;
      }
      cout<<"-------------"<<endl;
}


void Image::sauver(const string & filename)const {
    ofstream fichier (filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for(unsigned int y=0; y<dimy; ++y)
        for(unsigned int x=0; x<dimx; ++x) {
            Pixel& pix = getPix(x,y);
            fichier << pix.getRouge() << " " << pix.getVert() << " " << pix.getBleu() << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string & filename) {
    ifstream fichier (filename.c_str());
    assert(fichier.is_open());
  unsigned int r,g,b;
  string mot;
  dimx = dimy = 0;
  fichier >> mot >> dimx >> dimy >> mot;
  assert(dimx > 0 && dimy > 0);
  if (tab != NULL) delete [] tab;
  tab = new Pixel [dimx*dimy];
    for(unsigned int y=0; y<dimy;++y){
        for(unsigned int x=0; x<dimx;++x) {
            fichier >> r>> g>> b ;
            Pixel couleur(r,g,b);
            setPix(x,y,couleur);
            
        }

    }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole(){
    cout << dimx << " " << dimy << endl;
    for(unsigned int y=0; y<dimy; ++y) {
        for(unsigned int x=0; x<dimx;++x) {
            Pixel pix =getPix(x,y);
            cout << pix.getRouge() << " " << pix.getVert() << " " << pix.getBleu() << " ";
        }
        cout << endl;
    }
}

void Image::afficher(){
  afficherInit();
  afficherBoucle();
  afficherDetruit();
}
