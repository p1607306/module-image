#include "Pixel.h"
#include <cassert>

Pixel::Pixel(){
     r=0;
     g=0;
     b=0;
}

Pixel::Pixel(const unsigned int  nr,const unsigned int ng,const unsigned int nb){
	assert(nr>=0 && nr<=255);
	assert(ng>=0 && ng<=255);
	assert(nb>=0 && nb<=255);
     r=nr;
     g=ng;
     b=nb;
}

int Pixel::getRouge()const{
	int cr=r;
	return cr;
}

int Pixel::getVert()const{
	int cg=g;
	return cg;
}

int Pixel::getBleu()const{
	int cb=b;
	return cb;
}

void Pixel::setRouge(const unsigned int nr){
    assert(nr>=0 && nr<=255); 
	r=nr;
}

void Pixel::setVert(const unsigned int ng){
    assert(ng>=0 && ng<=255);   
	g=ng;
}

void Pixel::setBleu(const unsigned int nb){
    assert(nb>=0 && nb<=255);   
	b=nb;
}
